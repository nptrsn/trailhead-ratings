## Welcome to the Trailhead Mascot Ratings

Given the simple nature of this ratings system, I've kept things lightweight. 
While it's always nice to flex those CSS preprocessors, ES6 transpiler and Redux database skills, we're going to keep things agile with plain CSS (don't worry, there's a few variables to play with), and vanilla JavaScript.
**Edit**: In a perfect world, time is infinite. In ours, there's a little jQuery... ;p

1. For sake of time and ease, just pull this repo and fire up index.html in your browser
2. Rate away!
3. Don't be too harsh. Mascots have feelings too.

---

## Styling

Nothing too wild here. If anything, this should give us a great reminder why modular styles and preprocessors are a modern luxury.

A couple notes:

1. Global variables are declared just beneath the fonts, at the top. If branding changes, we can easily swap out fonts or colors with one line.
2. Media queries are declared immediately beneath their appropriate elements to save time and eye scrolling pain.
3. Our rating star sprites original properties are declared beginning at line 139. However, they're mostly controlled via our ***scripts.js*** file
4. The "overall" sprite and stars share styles with their counterparts. However, they do not share the same ***class name***. This is because the user is not voting on the overall rating. That rating is automatically calculated. Thus we do not want the standard sprite hover behavior on the overall sprite to trick the user into thinking they're voting on the overall ranking itself.

---

## JavaScript

The JS is divided into two main sections. First, we establish mascot objects to serve as a local "database" since we are not connected to a server. Second, we have our functions. A couple notes;

1. We're rounding our numbers up to boost the mascots' feelings. Thus, while the "overall rating" provides a more accurate integer to the hundreds (compared to our visual star ratings), the stars will round up. If we wanted, we could change **lines 79 and 111* to either Math.floor to round down.
2. You will notice repeated use of the eval() method. This is so we can pull the HTML's data attributes, as strings, to access their corresponding object. Note the differentiation between a data-mascot's attribute like "astro", which is just a string, and the actual astro JavaScript object. Eval() is our buddy here.
3. Our setDOM() method runs on initial page load to populate the HTML with our dummy mascot data. It also runs on when the stars are clicked.

---

## Happy Rating!