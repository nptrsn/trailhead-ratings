// Establish initial mascot objects
let astro, cloudy, codey, einstein = {
  cuteness: 0,
  intelligence: 0,
  trailSkills: 0,
  overall: 0
};

// Populating some dummy data to start
astro = {
  name: 'astro',
  cuteness: 10,
  intelligence: 6,
  trailSkill: 6,
  overall: 3.66,
  votes: 6
}
cloudy = {
  name: 'cloudy',
  cuteness: 15,
  intelligence: 9,
  trailSkill: 15,
  overall: 4.33,
  votes: 9
}
codey = {
  name: 'codey',
  cuteness: 20,
  intelligence: 16,
  trailSkill: 20,
  overall: 4.65,
  votes: 12
}
einstein = {
  name: 'einstein',
  cuteness: 1,
  intelligence: 5,
  trailSkill: 1,
  overall: 2.33,
  votes: 3
}

// jQuery Functions

$(document).ready( () => {
  // TABS
  $('.tab').hover( function () {
    $(this).addClass('tab-hover');
  });
  $( ".tab" ).mouseleave( function () {
    $(this).removeClass('tab-hover');
  });
  $('.tab').click( function() {
    let mascot = $(this).data('tab');
    $('.mascot').css({'display':'none'});
    $('.mascot-' + mascot).css({'display':'block'});
  });

  // SPRITE
  $('.star').hover( function () {
    let star = $(this).data('star');
    let trait = $(this).data('trait');
    let background = 108 - (star * 18);
    $('.sprite-' + trait).css({'background-position-y':background});
  })
  // Let's hope someone throws the ES6 fat arrow/'this' question here!
  $( ".star" ).mouseleave( () => {
    $('.sprite').css({'background-position-y':0});
    let trait = '';
  });

  // VOTING AND SCORE FUNCTIONS

  // Set DOM based on data
  let setDOM = function (mascot) {
    let score = eval(mascot['overall']);
    $('.overall-score-' + mascot.name).text(score);
    $('.total-votes-' + mascot.name).text(mascot.votes);
    let theInteger = Math.round(mascot.overall);
    switch (theInteger) {
      case 0 :
        $('.sprite-overall-' + mascot.name).css({'background-position-y':'0'});
      break;
      case 1 :
        $('.sprite-overall-' + mascot.name).css({'background-position-y':'90px'});
      break;
      case 2 :
        $('.sprite-overall-' + mascot.name).css({'background-position-y':'72px'});
      break;
      case 3 :
        $('.sprite-overall-' + mascot.name).css({'background-position-y':'54px'});
      break;
      case 4 :
        $('.sprite-overall-' + mascot.name).css({'background-position-y':'36px'});
      break;
      case 5 :
        $('.sprite-overall-' + mascot.name).css({'background-position-y':'18px'});
      break;
    }
  };

  // Set DOM stars on initial load
  setDOM(astro);
  setDOM(codey);
  setDOM(cloudy);
  setDOM(einstein);

  // Calculate new scores
  function score(mascot) {
    let newScore = (mascot.cuteness + mascot.intelligence + mascot.trailSkill) / mascot.votes;
    Math.round(10 * newScore) / 10; 
    mascot.overall = newScore.toFixed(2);
  }

  // Clicking on stars updates vote scores
  $('.star').click( function (){
    let mascot = eval($(this).data('mascot'));
    let trait = $(this).data('trait');
    let star = $(this).data('star');
    mascot.votes = eval(mascot['votes']);
    mascot.votes++;
    mascot[trait] = eval(mascot[trait]) + star;
    score(mascot);
    setDOM(mascot);
  })
});
